<?php

namespace App\DataFixtures;

use App\Entity\MySubObject;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\MyObject;

class AppFixtures extends Fixture
{
    private function loadMySubProject(ObjectManager $manager)
    {
        for ($i = 0; $i < 10; $i++) {
            $object = new MySubObject(MySubObject::$statuses[$i % 3]);
            $object->setName("Subname" . $i);
            $object->setNumber($i);
            $manager->persist($object);
        }

        $manager->flush();
    }

    private function addSubProjects(ObjectManager $manager, MyObject $object)
    {
        $subObjects = $manager->getRepository('App\Entity\MySubObject')->findAll();
        $n = random_int(1, 10);

        for ($i = 0; $i < $n; $i++) {
            $random = random_int(0, count($subObjects) - 1);
            $subObject = $subObjects[$random];
            if(!$object->getSubObjects()->contains($subObject)) {
                $object->addSubObject($subObject);
            }
        }
    }

    public function load(ObjectManager $manager)
    {
        $this->loadMySubProject($manager);


        for ($i = 0; $i < 150; $i++) {
            $object = new MyObject();
            $object->setName("The name" . $i);
            $object->setNumber($i);
            $this->addSubProjects($manager, $object);
            $manager->persist($object);
        }

        $manager->flush();
    }
}
