Symfony tools exercise
===================


# Step 1
* What does this code ?
* Configure the database
* Load fixtures with php bin/console doctrine:fixtures:load

# Step 2
In MyObjectController add a component to make "index" generating a more friendly view on http://localhost:8000/my-object/.
 
# Step 3
In index.html find a solution to set the number of "waiting" subobjects in the secund column.

# Step 4
1. Add a field CreatedAt in MyObject which should be initialize in constructor
2. Re-generate the fixtures.
3. Make the MyObject list with the more recent at the beginning.

# Step 5 (optional)
Add a form in the list to decide which status column should be display (either 'waiting', 'processed' or 'error')
